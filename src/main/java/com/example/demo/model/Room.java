package com.example.demo.model;

import java.util.Objects;

public class Room {
    private int id;
    private int size;
    private String description;
    private int price;

    public Room() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return getId() == room.getId() &&
                getSize() == room.getSize() &&
                getPrice() == room.getPrice() &&
                getDescription().equals(room.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSize(), getDescription(), getPrice());
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", size=" + size +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }

    public Room(int id, int size, String description, int price) {
        this.id = id;
        this.size = size;
        this.description = description;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
