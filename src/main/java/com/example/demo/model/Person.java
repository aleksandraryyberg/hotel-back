package com.example.demo.model;

public class Person {
    private int nr;
    private String name;
    private String surname;
    private int personalCode;
    private String status;

    public Person() {
    };

    public Person(int nr) {
        this.nr = nr;
    };

    public Person(int nr, String name, String surname, int personal_code, String status) {
        this.nr = nr;
        this.name = name;
        this.surname = surname;
        this.personalCode = personal_code;
        this.status = status;
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getPersonalCode() {
        return personalCode;
    }

    public void setPersonal_code(int personal_code) {
        this.personalCode = personal_code;
    }

    public  String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Person{" +
                "nr=" + nr +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", personal_code=" + personalCode + '\'' +
                ", status=" + status +
                '}';
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}



