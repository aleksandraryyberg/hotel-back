package com.example.demo.rest;

import com.example.demo.model.Booking;
import com.example.demo.model.OperationResultBooking;
import com.example.demo.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.demo.service.BookingService;

import java.util.List;


@RestController
@RequestMapping("/api/booking")
@CrossOrigin(origins = "http://localhost:4200")
public class BookingController {
    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private BookingService bookingService;

    @GetMapping("/allBookings")
    public List<Booking> getBooking() {
        return bookingService.getBooking();
    }

    @GetMapping("/{bookingNr}")
    public Booking getSingleBooking(@PathVariable("bookingNr") int bookingNr) {
        return bookingRepository.getBooking(bookingNr);
    }

    @DeleteMapping("/{bookingNr}")
    public ResponseEntity<OperationResultBooking> deleteBooking(@PathVariable("bookingNr") int bookingNr) {
        OperationResultBooking result = bookingService.deleteBooking(bookingNr);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("")
    public ResponseEntity<OperationResultBooking> updateBooking(@RequestBody Booking  booking){
        OperationResultBooking result = bookingService.updateBooking(booking);
        if (result.isSuccess()) {
            return new ResponseEntity<OperationResultBooking>(result, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<OperationResultBooking>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("")
    public ResponseEntity<OperationResultBooking> addBooking(@RequestBody Booking booking) {
        OperationResultBooking result = bookingService.addBooking(booking);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
}
