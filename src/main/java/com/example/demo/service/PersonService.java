package com.example.demo.service;

import com.example.demo.model.OperationResult;
import com.example.demo.model.Person;
import com.example.demo.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public OperationResult deletePerson(int personNr) {
        try {
            org.springframework.util.Assert.isTrue(personNr > 0, "Person isn't specified!");
            org.springframework.util.Assert.isTrue(personRepository.personExists(personNr), "The specified person does not exits!");
            personRepository.deletePerson(personNr);
            return new OperationResult(true, "OK", personNr);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResult(false, message, null);
        }
    }



    public OperationResult updatePerson(Person person) {
        try {
            org.springframework.util.Assert.isTrue(person != null, "Person's data does not exist!");
            org.springframework.util.Assert.isTrue(person.getName() != null && !person.getName().equals(""), "Person name not specified!");
            org.springframework.util.Assert.isTrue(person.getSurname() != null && !person.getSurname().equals(""), "Person's surname is not specified!");
            org.springframework.util.Assert.isTrue((person.getPersonalCode() != 0) && person.getPersonalCode() > 0, "Person's ID not specified!");

            List<Person> allPersonByName = personRepository.getPerson();
            // Assert.isTrue(allDogsittersByName.size() == 0 || allDogsittersByName.get(0).getNr() == dogsitters.getNr(),
            //        "Another company with the specified name already exists!");
            personRepository.updatePerson(person);
            return new OperationResult(true, "OK", person.getNr());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResult(false, message, null);
        }
    }

    public OperationResult addPerson(Person person) {
        if (personRepository.personExistsByCode(person.getPersonalCode())) {
            return new OperationResult(false, "Person with the same ID is already exists!", person.getNr());
        }
       // if (person.getName() == null && !person.getName().equals("")) {
        //    return new OperationResult(false, "Person's name is missing!", person.getNr());
      //  }
     //   if (person.getSurname() == null && !person.getSurname().equals("")) {
     //       return new OperationResult(false, "Person's surname is missing!", person.getNr());
     //   }
            // Assert.isTrue(allPersonByCode.size() == 0 || allPersonByCode.get(0).getPersonalCode() == person.getPersonalCode(),
            //  "Another person with same ID already exists!");
            // if(person.getPersonalCode() == personRepository.personExistsByCode(person)) {

            // }
            try {
                Assert.isTrue(person != null, "Person's data does not exist!");
                Assert.isTrue(person.getName() != null && !person.getName().equals(""), "Person's name not specified!");
                Assert.isTrue(person.getSurname() != null && !person.getSurname().equals(""), "Person's surname not specified!");
                Assert.isTrue(person.getPersonalCode() != 0 && person.getPersonalCode() > 0, "Person's code not specified!");
                long personNr = personRepository.addPerson(person);
                return new OperationResult(true, "OK", (int) personNr);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                String message = e.getMessage();
                return new OperationResult(false, message, null);
            }
        }

        public Person getPerson(int id) {
            return personRepository.getPerson(id);
        }
}
