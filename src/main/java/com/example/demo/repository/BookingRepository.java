package com.example.demo.repository;

import com.example.demo.model.Booking;
import com.example.demo.model.BookingPerson;
import com.example.demo.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


@Repository
public class BookingRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private PersonRepository personRepository;

    public List<Booking> getBooking() {
        return jdbcTemplate.query("select * from booking", mapBookingRows);
    }

    public Booking getBooking(int bookingNr) {
        List <Booking> bookingList = jdbcTemplate.query("select * from booking where bookingNr = ?", new Object[]{bookingNr}, mapBookingRows);
        Booking curBooking = bookingList.get(0);
        List <BookingPerson> bookingPerson = jdbcTemplate.query("select * from bookingperson where bookingNr = ?", new Object[]{bookingNr}, mapBookingPersonRows);
        List<Person> personsList = new ArrayList<>(bookingPerson.size());
        for(BookingPerson bPerson: bookingPerson ) {
            Person newPerson = personRepository.getPerson(bPerson.getId());
            personsList.add(newPerson);
        }
        Person[] personArray1 = new Person[personsList.size()];
        Person[] personArray2 = personsList.toArray(personArray1);
        curBooking.setPersons(personsList.toArray(personArray2));
        return curBooking;
    }
    public boolean bookingExists(int bookingNr) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(bookingNr) from booking where bookingNr = ?",
                new Object[]{bookingNr},
                Integer.class
        );
        return count != null && count > 0;
    }

    private RowMapper<Booking> mapBookingRows = (rs, rowNum) -> {
        Booking booking = new Booking();
        booking.setBookingNr(rs.getInt("bookingNr"));
        booking.setStartDate(rs.getDate("startDate"));
        booking.setEndDate(rs.getDate("endDate"));
        booking.setPersonsC(rs.getInt("personsC"));
        booking.setRoom(rs.getInt("room"));
        String persons = rs.getString("persons");
        Person[] personArray = String2PersonArray(persons);
        //TODO get person data from SQL

        booking.setPersons(personArray);
        return booking;
    };

    private RowMapper<BookingPerson> mapBookingPersonRows = (rs, rowNum) -> {
        BookingPerson bookingPerson = new BookingPerson();
        bookingPerson.setId(rs.getInt("Id"));
        bookingPerson.setBookingId(rs.getInt("bookingId"));
        bookingPerson.setPersonId(rs.getInt("personId"));
        //TODO get person data from SQL
        return bookingPerson;
    };

    public void updateBooking (Booking booking) {
        jdbcTemplate.update(
                "update booking set startDate, set endDate = ?, set personsC = ?, set room = ?, set persons = ?,  where bookingNr = ?",
                booking.getStartDate(), booking.getEndDate(), booking.getPersonsC(), booking.getRoom(), booking.getPersons(), booking.getBookingNr()
        );
    }

    public void deleteBooking(int bookingNr) {
        jdbcTemplate.update("delete from booking where bookingNr = ?", bookingNr);
    }

    public int addBooking(Booking booking) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(
                    "insert into booking (`startDate`, `endDate`,  `personsC`, `room`, `persons`) values (?, ?, ?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setDate(1, booking.getStartDate());
            ps.setDate(2, booking.getEndDate());
            ps.setInt(3, booking.getPersonsC());
            ps.setInt(4, booking.getRoom());
            ps.setString(5, PersonArray2String(booking.getPersons())); //personArray to String
            return ps;
        }, keyHolder);

        int curBookingId = keyHolder.getKey().intValue();
        Person[] persons = booking.getPersons();
        for(Person person: persons) {
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(
                        "insert into bookingperson (`bookingId`, `personId`) values (?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, curBookingId);
                ps.setInt(2, person.getNr());
                return ps;
            }, keyHolder);
        }

        return curBookingId;

    }

    private Person[] String2PersonArray(String personListString) {
        if (personListString == null || personListString.length() < 1) {
            return new Person[0];
        }
        String[] personNrArray = personListString.split(";");
        ArrayList<Person> personList = new ArrayList<Person>();
        for (String person : personNrArray) {
            personList.add(new Person(Integer.parseInt(person)));
        }
        Person[] personArrayTmp = new Person[personList.size()];
        Person[] personArray = personList.toArray(personArrayTmp);
        return personArray;
    }

    private String PersonArray2String(Person[] personArray) {
        String personIdList = new String();
        for(Person person: personArray) {
            personIdList = personIdList.concat(String.valueOf(person.getNr()));
            personIdList = personIdList.concat(";");
        }
        return personIdList;
    }
}
