package com.example.demo.rest;

import com.example.demo.model.BookingPerson;
import com.example.demo.model.OperationResultBookingPerson;
import com.example.demo.service.BookingPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/bookingperson")
@CrossOrigin(origins = "http://localhost:4200")
public class BookingPersonController {

    @Autowired
    private BookingPersonService bookingPersonService;

    @GetMapping("/allBookingPerson")
    public List<BookingPerson> getBookingPerson() {
        return bookingPersonService.getBookingPerson();
    }

    @GetMapping("/{id}")
    public BookingPerson getSingleBookingPerson(@PathVariable("id") int id) {
        return bookingPersonService.getBookingPerson(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<OperationResultBookingPerson> deleteBookingPerson(@PathVariable("id") int id) {
        OperationResultBookingPerson result = bookingPersonService.deleteBookingPerson(id);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("")
    public ResponseEntity<OperationResultBookingPerson> updateBookingPerson(@RequestBody BookingPerson bookingPerson){
        OperationResultBookingPerson result = bookingPersonService.updateBookingPerson(bookingPerson);
        if (result.isSuccess()) {
            return new ResponseEntity<OperationResultBookingPerson>(result, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<OperationResultBookingPerson>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("")
    public ResponseEntity<OperationResultBookingPerson> addBookingPerson(@RequestBody BookingPerson bookingPerson) {
        OperationResultBookingPerson result = bookingPersonService.addBookingPerson(bookingPerson);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
}
