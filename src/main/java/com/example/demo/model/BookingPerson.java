package com.example.demo.model;

public class BookingPerson {
    private int id;
    private int bookingId;
    private int personId;


    public BookingPerson() {
    };

    public BookingPerson(int id, int bookingId, int personId) {
        this.id = id;
        this.bookingId = bookingId;
        this.personId = personId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }
}
