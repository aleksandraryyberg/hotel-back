DROP TABLE IF EXISTS `room`;
DROP TABLE IF EXISTS `person`;
DROP TABLE IF EXISTS `booking`;

CREATE TABLE `booking` (
                           `bookingNr` INT(10) NOT NULL AUTO_INCREMENT,
                           `startDate` DATE NOT NULL,
                           `endDate` DATE NOT NULL,
                           `personsC` INT(18) NOT NULL,
                           `room` INT(18) NOT NULL,
                           `persons` VARCHAR(1000) NULL DEFAULT NULL,
                           INDEX `Index 1` (`bookingNr`),
                           INDEX `FK_room` (`room`),
                           CONSTRAINT `FK_room` FOREIGN KEY (`room`) REFERENCES `room` (`id`)
);


CREATE TABLE IF NOT EXISTS `room` (
                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                      `size` int(11) NOT NULL,
                                      `description` varchar(200) COLLATE utf8mb4_estonian_ci NOT NULL DEFAULT '',
                                      `price` int(11) NOT NULL DEFAULT 0,
                                      PRIMARY KEY (`id`));

CREATE TABLE IF NOT EXISTS `person` (
                                        `name` varchar(50) COLLATE utf8mb4_estonian_ci NOT NULL,
                                        `surname` varchar(50) COLLATE utf8mb4_estonian_ci NOT NULL,
                                        `personal_code` int(11) NOT NULL DEFAULT 0,
                                        `status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_estonian_ci');

CREATE TABLE `bookingperson` (
                                 `id` INT(10) NOT NULL AUTO_INCREMENT,
                                 `bookingId` INT(10) UNSIGNED NOT NULL,
                                 `personId` INT(11) NOT NULL,
                                 PRIMARY KEY (`id`),
                                 INDEX `FK_bookingperson_booking` (`bookingId`),
                                 INDEX `FK_bookingperson_person` (`personId`),
                                 CONSTRAINT `FK_bookingperson_booking` FOREIGN KEY (`bookingId`) REFERENCES `booking` (`bookingNr`),
                                 CONSTRAINT `FK_bookingperson_person` FOREIGN KEY (`personId`) REFERENCES `person` (`nr`)
);
