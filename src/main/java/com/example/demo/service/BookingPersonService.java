package com.example.demo.service;

import com.example.demo.model.BookingPerson;
import com.example.demo.model.OperationResultBookingPerson;
import com.example.demo.model.Person;
import com.example.demo.repository.BookingPersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookingPersonService {

    @Autowired
    private BookingPersonRepository bookingPersonRepository;

    @Autowired
    private PersonService personService;

    public OperationResultBookingPerson deleteBookingPerson(Integer id) {
        try {
            org.springframework.util.Assert.isTrue(id != null, "Booking ID is not specified!");
            org.springframework.util.Assert.isTrue(bookingPersonRepository.bookingPersonExists(id), "The specified booking does not exits!");
            bookingPersonRepository.deleteBookingPerson(id);
            return new OperationResultBookingPerson(true, "OK", id);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResultBookingPerson(false, message, null);
        }
    }


    public OperationResultBookingPerson updateBookingPerson(BookingPerson bookingPerson) {
        try {
            org.springframework.util.Assert.isTrue(bookingPerson != null, "BookingPerson data does not exist!");
            org.springframework.util.Assert.isTrue(bookingPerson.getBookingId() != 0 , "Booking ID is not specified!");
            org.springframework.util.Assert.isTrue(bookingPerson.getPersonId() != 0, "Person's ID is not specified!");

            List<BookingPerson> allBookingPersonById = bookingPersonRepository.getBookingPerson();
            Assert.isTrue(allBookingPersonById.size() == 0 || allBookingPersonById.get(0).getId() == bookingPerson.getId(),
                    "Another booking with the specified id-number already exists!");
            bookingPersonRepository.updateBookingPerson(bookingPerson);
            return new OperationResultBookingPerson(true, "OK", bookingPerson.getId());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResultBookingPerson(false, message, null);
        }
    }
    public OperationResultBookingPerson addBookingPerson(BookingPerson bookingPerson) {
        try {
            Assert.isTrue(bookingPerson != null, "BookingPerson's data does not exist!");
            Assert.isTrue(bookingPerson.getBookingId() != 0 , "Booking ID is not specified!");
            Assert.isTrue(bookingPerson.getPersonId() != 0, "Person's ID is not specified!");

            int bookingPersonId = bookingPersonRepository.addBookingPerson(bookingPerson);
            return new OperationResultBookingPerson(true, "OK", bookingPersonId);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResultBookingPerson(false, message, null);
        }
    }

    public BookingPerson getBookingPerson(int id) {
       BookingPerson bookingPerson = bookingPersonRepository.getBookingPerson(id);
        return bookingPerson;
    }

    public List<Person> getBookingPersonsList(int id) {

        List<Person> personsList = new ArrayList<Person>();

        List<BookingPerson> bookingPersonList = bookingPersonRepository.getBookingPersonsList(id);
        for(BookingPerson bookingPerson: bookingPersonList) {
            personsList.add(personService.getPerson(bookingPerson.getId()));
        }

        return personsList;
    }
    public List<BookingPerson> getBookingPerson() {
        return bookingPersonRepository.getBookingPerson();
    }

    public List<BookingPerson> getSingleBookingPerson() {
        return bookingPersonRepository.getBookingPerson();
    }
}
