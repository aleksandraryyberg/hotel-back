package com.example.demo.model;

public class OperationResultBookingPerson {
    private boolean isSuccess;
    private String message;
    private Integer id;

    public OperationResultBookingPerson(boolean isSuccess, String message, Integer id) {
        this.isSuccess = isSuccess;
        this.message = message;
        this.id = id;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public int getId() {
        return id;
    }
}
