package com.example.demo.repository;

import com.example.demo.model.BookingPerson;
import com.example.demo.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


@Repository
public class BookingPersonRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<BookingPerson> getBookingPerson() {
        return jdbcTemplate.query("select * from bookingperson", mapBookingPersonRows);

    }

    public BookingPerson getBookingPerson(int id) {
        List<BookingPerson> bookingPerson = jdbcTemplate.query("select * from bookingperson where id = ?", new Object[]{id}, mapBookingPersonRows);
        return bookingPerson.size() > 0 ? bookingPerson.get(0) : null;
    }

    public List<BookingPerson> getBookingPersonsList(int id) {

        List<BookingPerson> bookingPersonsList = jdbcTemplate.query("select * from bookingperson where bookingId = ?", new Object[]{id}, mapBookingPersonRows);

        return bookingPersonsList;
    }

    public boolean bookingPersonExists(int id) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(id) from bookingperson where id = ?",
                new Object[]{id},
                Integer.class
        );
        return count != null && count > 0;
    }

    private RowMapper<BookingPerson> mapBookingPersonRows = (rs, rowNum) -> {
        BookingPerson bookingPerson = new BookingPerson();
        bookingPerson.setId(rs.getInt("Id"));
        bookingPerson.setBookingId(rs.getInt("bookingId"));
        bookingPerson.setPersonId(rs.getInt("personId"));
        return bookingPerson;
    };

    public void updateBookingPerson(BookingPerson bookingPerson) {
        jdbcTemplate.update(
                "update bookingperson set bookingId, set personId = ?  where id = ?",
                bookingPerson.getBookingId(), bookingPerson.getPersonId(), bookingPerson.getId()
        );
    }

    public void deleteBookingPerson(int id) {
        jdbcTemplate.update("delete from bookingperson where id = ?", id);
    }

    public int addBookingPerson(BookingPerson bookingPerson) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(
                    "insert into bookingperson (`bookingId`, `personId`) values (?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, bookingPerson.getBookingId());
            ps.setInt(2, bookingPerson.getPersonId());
            return ps;
        }, keyHolder);
        return keyHolder.getKey().intValue();
    }
}

