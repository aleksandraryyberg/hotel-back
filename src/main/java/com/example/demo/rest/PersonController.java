package com.example.demo.rest;

import com.example.demo.model.OperationResult;
import com.example.demo.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.demo.repository.PersonRepository;
import com.example.demo.service.PersonService;

import java.util.List;


@RestController
@RequestMapping("/api/person")
@CrossOrigin(origins = "http://localhost:4200")
public class PersonController {
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonService personService;

    @GetMapping("/all")
    public List<Person> getPerson() {
        return personRepository.getPerson();
    }

    @GetMapping("/{personNr}")
    public Person getSinglePerson(@PathVariable("personNr") int nr) {
        return personRepository.getPerson(nr);
}

    @DeleteMapping("/{personNr}")
    public ResponseEntity<OperationResult> deletePerson(@PathVariable("personNr") int nr) {
        OperationResult result = personService.deletePerson(nr);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("")
    public ResponseEntity<OperationResult> updatePerson(@RequestBody Person person){
        OperationResult result = personService.updatePerson(person);
        if (result.isSuccess()) {
            return new ResponseEntity<OperationResult>(result, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<OperationResult>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("")
    public ResponseEntity<OperationResult> addPerson(@RequestBody Person person) {
        OperationResult result = personService.addPerson(person);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
}
