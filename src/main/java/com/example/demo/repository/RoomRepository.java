package com.example.demo.repository;

import com.example.demo.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;


@Repository
public class RoomRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Room> getRoom() {
        return jdbcTemplate.query("select * from room", mapRoomRows);
    }

    public Room getRoom(int id) {
        List <Room> room = jdbcTemplate.query("select * from room where id = ?", new Object[]{id}, mapRoomRows);
        return room.size() > 0 ? room.get(0) : null;
    }
    public boolean roomExists(int roomId) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(id) from room where id = ?",
                new Object[]{roomId},
                Integer.class
        );
        return count != null && count > 0;
    }

    private RowMapper<Room> mapRoomRows = (rs, rowNum) -> {
        Room room = new Room();
        room.setId(rs.getInt("id"));
        room.setSize(rs.getInt("size"));
        room.setDescription(rs.getString("description"));
        room.setPrice(rs.getInt("price"));
        return room;
    };

    public void updateRoom (Room room) {
        jdbcTemplate.update(
                "update room set size = ?, set description = ?, set price = ?  where id  = ?",
                room.getSize(), room.getDescription(), room.getPrice(), room.getId()
        );
    }

    public void deleteRoom(int roomId) {
        jdbcTemplate.update("delete from room where id = ?", roomId);
    }

    public int addRoom(Room room) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(
                    "insert into room (`size`, `description`,  `price`) values (?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, room.getSize());
            ps.setString(2, room.getDescription());
            ps.setInt(3, room.getPrice());
            return ps;
        }, keyHolder);
        return keyHolder.getKey().intValue();

    }

}
