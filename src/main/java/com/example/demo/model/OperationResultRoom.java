package com.example.demo.model;

public class OperationResultRoom {
    private boolean isSuccess;
    private String message;
    private Integer roomId;

    public OperationResultRoom(boolean isSuccess, String message, Integer roomId) {
        this.isSuccess = isSuccess;
        this.message = message;
        this.roomId = roomId;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public Integer getRoomId() {
        return roomId;
    }
}

