package com.example.demo.service;

import com.example.demo.model.*;
import com.example.demo.repository.BookingRepository;
import com.example.demo.repository.PersonRepository;
import com.example.demo.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookingService {

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private RoomService roomService;

    @Autowired RoomRepository roomRepository;

    @Autowired
    BookingPersonService bookingPersonService;

    public OperationResultBooking deleteBooking(int bookingNr) {
        try {
            org.springframework.util.Assert.isTrue(bookingNr > 0, "Booking isn't specified!");
            org.springframework.util.Assert.isTrue(bookingRepository.bookingExists(bookingNr), "The specified booking does not exits!");
            bookingRepository.deleteBooking(bookingNr);
            return new OperationResultBooking(true, "OK", bookingNr);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResultBooking(false, message, null);
        }
    }

    public OperationResultBooking updateBooking(Booking booking) {
        try {
            org.springframework.util.Assert.isTrue(booking != null, "Booking's data does not exist!");
            org.springframework.util.Assert.isTrue(booking.getStartDate() != null && !booking.getStartDate().equals(""), "Booking is not specified!");
            org.springframework.util.Assert.isTrue(booking.getEndDate() != null && !booking.getEndDate().equals(""), "Booking is not specified!");
            org.springframework.util.Assert.isTrue((booking.getPersonsC() != 0) && booking.getPersonsC() > 0, "the number of persons is not specified!");
            org.springframework.util.Assert.isTrue((booking.getRoom() != 0) && booking.getRoom() > 0, "Room nr is not specified!");
            org.springframework.util.Assert.isTrue(booking.getPersons() != null && booking.getPersons().length > 0, "Room nr is not specified!");
            List<Booking> allBookingByNr = bookingRepository.getBooking();
            // Assert.isTrue(allDogsittersByName.size() == 0 || allDogsittersByName.get(0).getNr() == dogsitters.getNr(),
            //        "Another company with the specified name already exists!");
            bookingRepository.updateBooking(booking);
            return new OperationResultBooking(true, "OK", booking.getBookingNr());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResultBooking(false, message, null);
        }
    }
    public OperationResultBooking addBooking(Booking booking) {
        if(booking.getStartDate() == null && !booking.getStartDate().equals("")) {
            return new OperationResultBooking(false, "Start date is missing!", booking.getBookingNr());
        }
        if(booking.getStartDate() == null && !booking.getEndDate().equals("")) {
            return new OperationResultBooking(false, "End date is missing!", booking.getBookingNr());
        }
        if(booking.getPersonsC() <= 0) {
            return new OperationResultBooking(false, "Number of clients is missing!", booking.getBookingNr());
        }
        if(booking.getRoom() == 0) {
            return new OperationResultBooking(false, "Please select the room!", booking.getBookingNr());
        }
        if(booking.getPersons() == null) {
            return new OperationResultBooking(false, "Please select persons!", booking.getBookingNr());
        }

        try {
            Assert.isTrue(booking != null, "Booking's data does not exist!");
            Assert.isTrue(booking.getStartDate() != null && !booking.getStartDate().equals(""), "Booking is not specified!");
            Assert.isTrue(booking.getEndDate() != null && !booking.getEndDate().equals(""), "Booking is not specified!");
            Assert.isTrue(booking.getPersonsC() != 0 && booking.getPersonsC() > 0, "Booking is not specified!");
            Assert.isTrue(booking.getRoom() != 0 && booking.getRoom() > 0, "Booking is not specified!");
            Assert.isTrue(booking.getPersons() != null && booking.getPersons().length > 0, "Room nr is not specified!");

            Room room = roomService.getRoom(booking.getRoom());
            int maxPersonsInRoom = room.getSize();
            if (maxPersonsInRoom < booking.getPersons().length) {
                System.out.println("==== ERROR ");
                return new OperationResultBooking(false, "To many persons for this room", booking.getBookingNr());
            }


            long boookingNr = bookingRepository.addBooking(booking);
            return new OperationResultBooking(true, "OK", (int) boookingNr);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResultBooking(false, message, null);
        }
    }

    public List<Booking> getBooking() {
        List<Booking> bookingList = new ArrayList<Booking>();
        bookingList = bookingRepository.getBooking();
        List<Booking> updateBookingList = new ArrayList<Booking>();

        for (Booking booking:  bookingList) {
            Person[] bookingPersons = booking.getPersons();
            List<Person> updateBookingPersons = new ArrayList<Person>();


            /* List<Person> bookingPersonsList = bookingPersonService.getBookingPersonsList(booking.getBookingNr());
            Person[] personArray = new Person[bookingPersonsList.size()];
            personArray = bookingPersonsList.toArray(personArray);
            booking.setPersons(personArray);*/

            for(Person person: bookingPersons) {
                Person personaleData = personRepository.getPerson(person.getNr());
                person.setName(personaleData.getName());
                person.setSurname(personaleData.getSurname());
                person.setPersonal_code(personaleData.getPersonalCode());
                updateBookingPersons.add(person);
            }
            Person[] tmpersons = new Person[updateBookingPersons.size()];
            booking.setPersons(updateBookingPersons.toArray(tmpersons));

            updateBookingList.add(booking);
        }
        return updateBookingList;
    }
}
