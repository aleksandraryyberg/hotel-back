INSERT INTO `person` (`name`, `surname`, `personal_code`, `status`) VALUES
('Aleksandra', 'Ryyberg', 4840818),
('Marko', 'Lucky', 3790505),
('Vitautas', 'Paukshte', 3663456),
('Marija', 'Reality', 4890909),
('Tamara', 'Reha', 4560701),
('Marko', 'Markus', 3330524);


INSERT INTO `room` (`id`, `size`, `description`, `price`) VALUES
(1, 1, 'single', 50),
(2, 1, 'single-premium', 75),
(3, 1, 'single-lux', 100),
(4, 2, 'double', 75),
(5, 2, 'double-premium', 125),
(6, 2, 'double-lux', 150),
(7, 3, 'triple', 150),
(8, 3, 'triple XXL', 175),
(9, 3, 'triple-lux', 200);

INSERT INTO `booking` (`bookingNr`, `startDate`, `endDate`, `personsC`, `room`, `persons`) VALUES
(0000000001, '2020-10-29', '2020-10-30', 2, 4, 1 );

INSERT INTO `bookingperson` (`id`, `bookingId`, `personId`) VALUES
(16, 43, 2);

