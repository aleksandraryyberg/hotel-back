package com.example.demo.rest;

import com.example.demo.model.OperationResultRoom;
import com.example.demo.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.demo.repository.RoomRepository;
import com.example.demo.service.RoomService;


import java.util.List;


@RestController
@RequestMapping("/api/room")
@CrossOrigin(origins = "http://localhost:4200")
public class RoomController {
    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private RoomService roomService;

    @GetMapping("/allRooms")
    public List<Room> getRoom() {
        return roomRepository.getRoom();
    }

    @GetMapping("/{roomId}")
    public Room getSingleRoom(@PathVariable("roomId") int id) {
        System.out.println("==== /room/id");
        return roomRepository.getRoom(id);
    }


    @DeleteMapping("/{roomId}")
    public ResponseEntity<OperationResultRoom> deleteRoom(@PathVariable("roomId") int id) {
        OperationResultRoom result = roomService.deleteRoom(id);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/updateRoom")
    public ResponseEntity<OperationResultRoom> updateRoom(@RequestBody Room room){
        OperationResultRoom result = roomService.updateRoom(room);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("/addRoom")
    public ResponseEntity<OperationResultRoom> addRoom(@RequestBody Room room) {
        OperationResultRoom result = roomService.addRoom(room);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
}

