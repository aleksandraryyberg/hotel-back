package com.example.demo.model;

public class OperationResultBooking {
    private boolean isSuccess;
    private String message;
    private Integer bookingNr;

    public OperationResultBooking(boolean isSuccess, String message, Integer bookingNr) {
        this.isSuccess = isSuccess;
        this.message = message;
        this.bookingNr = bookingNr;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public int getBookingNr() {
        return bookingNr;
    }
}
