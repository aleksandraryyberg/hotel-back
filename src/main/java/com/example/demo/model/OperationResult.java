package com.example.demo.model;

public class OperationResult {
    private boolean isSuccess;
    private String message;
    private Integer personNr;

    public OperationResult(boolean isSuccess, String message, Integer personNr) {
        this.isSuccess = isSuccess;
        this.message = message;
        this.personNr = personNr;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public int getPersonNr() {
        return personNr;
    }
}

