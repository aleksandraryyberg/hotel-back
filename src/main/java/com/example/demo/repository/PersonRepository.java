package com.example.demo.repository;

import com.example.demo.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class PersonRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Person> getPerson() {
        return jdbcTemplate.query("select * from person where status= 'A'", mapPersonRows);
    }

    public Person getPerson(int nr) {
        List <Person> person = jdbcTemplate.query("select * from person where nr = ?", new Object[]{nr}, mapPersonRows);
        return person.size() > 0 ? person.get(0) : null;
    }

    public boolean personExists(int personNr) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(nr) from person where nr = ?",
                new Object[]{personNr},
                Integer.class
        );
        return count != null && count > 0;
    }

    public boolean personExistsByCode(int personal_code) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(nr) from person where personal_code = ?",
                new Object[]{personal_code},
                Integer.class
        );
        return count != null && count > 0;
    }
    private RowMapper<Person> mapPersonRows = (rs, rowNum) -> {
        Person person = new Person();
        person.setNr(rs.getInt("nr"));
        person.setName(rs.getString("name"));
        person.setSurname(rs.getString("surname"));
        person.setPersonal_code(rs.getInt("personal_code"));
        person.setStatus(rs.getString("status"));
        return person;
    };

    public List<Person> getAllPersonByName(String name) {
        return jdbcTemplate.query(
                "select * from person where `name` like ?",
                new Object[]{"%" + name + "%"},
                (row, number) -> {
                    return new Person(
                            row.getInt("nr"),
                            row.getString("name"),
                            row.getString("surname"),
                            row.getInt("personal_code"),
                            row.getString("status")
                    );
                });
    }

    public void updatePerson (Person person) {
            jdbcTemplate.update(
                    "update person set name = ?, surname = ?, personal_code = ?, status = ? where nr  = ?",
                    person.getName(), person.getSurname(), person.getPersonalCode(), "A", person.getNr()
            );
    }

    public void deletePerson(int personNr) {
        jdbcTemplate.update("update person set status= 'D' where nr = ?", personNr);
    }




   /* public List<Person> getAllPersonByName(String name) {
        return jdbcTemplate.query(
                "select * from person where `nr` like ?",
                new Object[]{"%" + nr + "%"},
                (row, number) -> {
                    return new Person(
                            row.getInt("nr"),
                            row.getString("nimi"),
                            row.getString("foto"),
                            row.getString("koht"),
                            row.getString("teenused"),
                            row.getInt("kogemus"),
                            row.getString("Kontakt"),
                            services
                            //announcementRepository.getAnnouncements(row.getInt("nr"))
                    );
                });
    }*/
        public long addPerson(Person person) {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(
                        "insert into person (name, surname, personal_code, status) values (?, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, person.getName());
                ps.setString(2, person.getSurname());
                ps.setInt(3, person.getPersonalCode());
                ps.setString(4, "A");
                return ps;
            }, keyHolder);
            return  ((Long)keyHolder.getKeyList().get(0).get("insert_id")).longValue();
        }
    }










