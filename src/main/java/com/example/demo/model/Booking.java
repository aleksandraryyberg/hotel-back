package com.example.demo.model;

import java.sql.Date;

public class Booking {
        private int bookingNr;
        private Date startDate;
        private Date endDate;
        private int personsC;
        private int room;
        private Person persons[];


        public Booking() {
        };

        public Booking(int bookingNr, Date startDate, Date endDate, int personsC, int room, Person[] persons) {
                this.bookingNr = bookingNr;
                this.startDate = startDate;
                this.endDate = endDate;
                this.personsC = personsC;
                this.room = room;
                this.persons = persons;

        }

        public int getBookingNr() {
                return bookingNr;
        }

        public void setBookingNr(int bookingNr) {
                this.bookingNr = bookingNr;
        }

        public Date getStartDate() {
                return startDate;
        }

        public void setStartDate(Date startDate) {
                this.startDate = startDate;
        }

        public Date getEndDate() {
                return endDate;
        }

        public void setEndDate(Date endDate) {
                this.endDate = endDate;
        }

        public int getPersonsC() {
                return personsC;
        }

        public void setPersonsC(int personsC) {
                this.personsC = personsC;
        }

        public int getRoom() {
                return room;
        }

        public void setRoom(int room) {
                this.room = room;
        }

        public Person[] getPersons() {
                return persons;
        }

        public void setPersons(Person[] persons) {
                this.persons = persons;
        }
}
