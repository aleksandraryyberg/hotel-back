package com.example.demo.service;

import com.example.demo.model.OperationResultRoom;
import com.example.demo.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import com.example.demo.repository.RoomRepository;

import java.util.List;

@Service
public class RoomService {

    @Autowired
    private RoomRepository roomRepository;

    public OperationResultRoom deleteRoom(Integer roomId) {
        try {
            org.springframework.util.Assert.isTrue(roomId != null, "Room's name is not specified!");
            org.springframework.util.Assert.isTrue(roomRepository.roomExists(roomId), "The specified room does not exits!");
            roomRepository.deleteRoom(roomId);
            return new OperationResultRoom(true, "OK", roomId);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResultRoom(false, message, null);
        }
    }


    public OperationResultRoom updateRoom(Room room) {
        try {
            org.springframework.util.Assert.isTrue(room != null, "Room data does not exist!");
            org.springframework.util.Assert.isTrue(room.getSize() != null && !room.getSize().equals(""), "Room's size not specified!");
            org.springframework.util.Assert.isTrue(room.getDescription() != null && !room.getDescription().equals(""), "Room's description not specified!");
            org.springframework.util.Assert.isTrue(room.getPrice() != 0, "Room's price not specified!");


            List<Room> allRoomsById = roomRepository.getRoom();
            Assert.isTrue(allRoomsById.size() == 0 || allRoomsById.get(0).getId() == room.getId(),
            "Another room with the specified id-number already exists!");
            roomRepository.updateRoom(room);
            return new OperationResultRoom(true, "OK", room.getId());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResultRoom(false, message, null);
        }
    }
    public OperationResultRoom addRoom(Room room) {
        try {
            Assert.isTrue(room != null, "Room's data does not exist!");
            Assert.isTrue(room.getSize() != null && !room.getSize().equals(""), "Room's size not specified!");
            Assert.isTrue(room.getDescription() != null && !room.getDescription().equals(""), "Rooms's description not specified!");

            int roomId = roomRepository.addRoom(room);
            return new OperationResultRoom(true, "OK", roomId);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResultRoom(false, message, null);
        }
    }

    public Room getRoom(int roomId) {
        Room room = roomRepository.getRoom(roomId);
        return room;
    }
}
